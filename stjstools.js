var Class = { 
	create:  function(methods) {   
	    var klass = function() {    
	        this.initialize.apply(this, arguments);          
	    };  
	    
	    for (var property in methods) { 
	       klass.prototype[property] = methods[property];
	    }
	          
	    if (!klass.prototype.initialize) klass.prototype.initialize = function(){};      
	    
	    klass.create = function(){
	    	return {};
	    };
	    return klass;    
	}
};

var SOAPMessage = Class.create({
	setParameter : function(){},
	post : function(){}
});

var XMLDocument = Class.create({
	getNodeText : function () {}
});

var _oldJson = JSON;
var JSON = Class.create(
	{
		encode: function(obj)
		{
			return _oldJson.stringify(obj);
		}
	});

var gs = { 
	log : function(a, b)
	{
		result = a;
		if(b){
			result += " [" + b + "]";
		}
		debug(result);
	},
	nowDateTime : function()
	{
		return (new Date).timeNow();
	},
	getProperty : function(){
		return "";
	}
};

var XMLHelper = Class.create();


Date.prototype.today = function () { 
    return ((this.getDate() < 10)?"0":"") + this.getDate() +"/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ this.getFullYear();
}

// For the time now
Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}
debug("Loaded stjstools.js");